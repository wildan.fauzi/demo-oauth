package com.id.demooauth2.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/users")
@Validated
@Slf4j
public class UserControllers {

	@Autowired
	private UserRepository repository;

	@Autowired
	private PasswordEncoder encoder;
	
	@GetMapping
	Page<AppUser> all(@PageableDefault(size = Integer.MAX_VALUE) Pageable pageable, OAuth2Authentication authentication) {
		String auth = (String) authentication.getUserAuthentication().getPrincipal();
		String role = authentication.getAuthorities().iterator().next().getAuthority();
		System.out.println(auth);
		System.out.println(role);
		
		if (role.equals(AppUser.Role.USER.name())) {
			return repository.findAllByEmail(auth, pageable);
		}
		
		return repository.findAll(pageable);
		
	}
}
