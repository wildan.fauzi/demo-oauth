package com.id.demooauth2.users;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Long>{
	Optional<AppUser> findByEmail(String email);
	
	Page<AppUser> findAllByEmail(String email, Pageable pageable);
	
	boolean existsByEmail(String email);
}
