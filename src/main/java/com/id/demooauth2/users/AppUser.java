package com.id.demooauth2.users;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter @Getter
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "email")}, name = "app_user")
public class AppUser{

	public enum Role {USER, ADMIN, USER_MANAGER}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@NotEmpty
	@Email
	private String email;
	
	@JsonIgnore
	@ToString.Exclude
	private String password;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private Role role;
	
}
