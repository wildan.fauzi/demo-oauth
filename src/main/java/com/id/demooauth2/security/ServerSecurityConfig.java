package com.id.demooauth2.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.id.demooauth2.errors.CustomAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
public class ServerSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	   protected void configure(HttpSecurity http) throws Exception {
	       http
	               .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	               .and()
	               .authorizeRequests()
	               .antMatchers("/api/signin/**").permitAll()
	               .antMatchers("/api/glee/**").hasAnyAuthority("ADMIN", "USER")
	               .antMatchers("/api/users/**").hasAuthority("ADMIN")
	               .antMatchers("/api/**").authenticated()
	               .anyRequest().authenticated()
	               ;      
	   }
	
}
