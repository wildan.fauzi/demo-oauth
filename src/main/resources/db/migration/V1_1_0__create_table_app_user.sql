CREATE TYPE role AS ENUM ('USER', 'ADMIN', 'USER_MANAGER');

CREATE TABLE app_user (
	id  bigserial not null, 
	email varchar(256) not null, 
	password varchar(256) not null,
	role Role not null,
	primary key (id)
);
